package com.telegram.bot.utils;

import com.telegram.bot.annotation.Name;
import com.telegram.bot.model.Person;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.CollectionUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class BuildTableUtils {

    public static <T extends Object> String buildTable(List<T> objects, Class<T> clazz) {
        try {
            if (CollectionUtils.isEmpty(objects)) {
                return "";
            }
            T t = objects.get(0);
            Field[] fields = t.getClass().getDeclaredFields();
            List<Integer> list = new ArrayList<>();
            for (Field field : fields) {
                Name name = field.getAnnotation(Name.class);
                list.add(name.value().length());
            }
            for (int m= 0 ; m< objects.size(); m++) {
                for (int k = 0; k < fields.length; k++) {
                    if (Objects.nonNull(fields[k].get(objects.get(m))) && (fields[k].get(objects.get(m)).toString().length() > list.get(k))) {
                        list.set(k, fields[k].get(objects.get(m)).toString().length());
                    }
                }
            }
            StringBuilder header = new StringBuilder();
            StringBuilder body = new StringBuilder();
            header.append(String.format("%1s", "|"));
            for (int k= 0 ; k <fields.length ; k++) {
                header.append(String.format("%-" + list.get(k) + "s %1s ", fields[k].getAnnotation(Name.class).value(), "|"));
            }
            String  line = StringUtils.leftPad("", header.length() - 1, "-");
            body.append("<pre>");
            body.append(line).append("\n").append(header).append("\n").append(line);
            for (T tx : objects) {
                body.append("\n");
                body.append(String.format("%1s", "|"));
                for (int k = 0; k < fields.length; k++) {
                    body.append(String.format("%-" + list.get(k) + "s %1s ", Objects.nonNull(fields[k].get(tx)) ? fields[k].get(tx).toString() : "", "|"));
                }
            }
            body.append("\n");
            body.append(line);
            body.append("</pre>");
            System.out.println(body);
            return body.toString();
        } catch (Exception e) {
            e.printStackTrace();
            return "Cannot export table";
        }
    }

}
