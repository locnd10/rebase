package com.telegram.bot.utils;

public class Constant {
    public static final String START_TEXT = "Nhập tên của bạn:";
    public static final String HELP = "Đây là các cú pháp có sẵn:\n/start - Bắt đầu\n/search - Chức năng tìm kiếm \n/order - Mua hàng \n/stop - Kết thúc";

    public static final String SEARCH = "Tìm kiếm đơn đơn hàng (order).\n Tìm kiếm thông tin (person).";
    public static final String STOP = "Kết thúc cuộc trò chuyện. \nCảm ơn bạn đã ghé thăm";
    public static final String ORDER = "Bạn muốn mua gì (^-^)";
    public static final String CHAT_STATES = "CHAT_STATESVVGD";
}
