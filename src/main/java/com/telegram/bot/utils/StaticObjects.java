package com.telegram.bot.utils;

import com.telegram.bot.model.Order;
import com.telegram.bot.model.Person;

import java.util.Arrays;
import java.util.List;

public class StaticObjects {
    public static List<Person> people = Arrays.asList( new Person("Tuấn", 19, "HCM", "091281"),
            new Person("Ngọc", 20, "Tây Ninh", "0192311"),
            new Person("Nam", 22, "Phú Nhuận", "0192322"));
    public static List<Order> orders = Arrays.asList( new Order("1", "Tuấn", "200.000.000"),
            new Order("2", "Tú", "100.000.000"),
            new Order("3", "Nam", "20.000.000"));




}
