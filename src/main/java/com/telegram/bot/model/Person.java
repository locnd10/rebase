package com.telegram.bot.model;

import java.sql.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.telegram.bot.annotation.Name;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@AllArgsConstructor
public class Person {
    @Name(value = "Tên")
    public String name;
    @Name(value = "Tuổi")
    public Integer age;
    @Name(value = "Thành phố")
    public String city;
    @Name(value = "mobile")
    public String phone;

}
