package com.telegram.bot.model;

import com.telegram.bot.annotation.Name;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Order {
    @Name("ID")
    public String id;
    @Name("Tên")
    public String name;
    @Name("Giá")
    public String price;
}
