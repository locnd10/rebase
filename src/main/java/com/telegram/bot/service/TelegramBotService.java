package com.telegram.bot.service;

import com.telegram.bot.utils.Constant;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.telegram.abilitybots.api.bot.AbilityBot;
import org.telegram.telegrambots.meta.api.objects.Update;

import java.util.Map;

@Service
@Slf4j
public class TelegramBotService extends AbilityBot {
    private final CommandHandlerService commandHandler;
    private final TextHandlerService sendTextService;
    private Map<String, Object> chatStates;

    public TelegramBotService(Environment environment, CommandHandlerService commandHandler, TextHandlerService sendTextService) {
        super(environment.getProperty("telegram.botToken"), environment.getProperty("telegram.botUsername"));
        chatStates = db().getMap(Constant.CHAT_STATES);
        this.commandHandler = commandHandler;
        this.sendTextService = sendTextService;
    }

    @Override
    public long creatorId() {
        return 0;
    }

    @Override
    public void onUpdateReceived(Update update) {
        log.info("onUpdateReceived {}", update.getMessage());
        if (update.getMessage().getText().startsWith("/")) {
            commandHandler.commandHandler(silent, sender, chatStates, update.getMessage().getText(), update.getMessage().getChatId());
        } else {
            sendTextService.textHandler(silent, sender, chatStates, update.getMessage().getText(), update.getMessage().getChatId());
        }
    }
}
