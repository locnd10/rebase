package com.telegram.bot.service;

import com.telegram.bot.model.Order;
import com.telegram.bot.model.Person;
import com.telegram.bot.utils.BuildTableUtils;
import com.telegram.bot.utils.Constant;
import com.telegram.bot.utils.StaticObjects;
import com.telegram.bot.utils.UserState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.Map;

@Slf4j
@Service
public class TextHandlerService {

    public void textHandler(SilentSender silentSender, MessageSender messageSender, Map<String, Object> chatStates, String content, Long chatId) {
        if (UserState.START.equals(chatStates.get(chatId.toString()))) {
            setName(silentSender, chatStates, chatId, content);
        }
        if (UserState.SEARCH.equals(chatStates.get(chatId.toString()))) {
            if ("order".equalsIgnoreCase(content)) {
                replyTableOrder(silentSender, chatStates, chatId);
            }
            if ("person".equalsIgnoreCase(content)) {
                replyTablePerson(silentSender, chatStates, chatId);
            }
        }

    }

    public void setName(SilentSender silentSender, Map<String, Object> chatStates, Long chatId, String name) {
        chatStates.put((chatId + "name"), name);
        silentSender.execute(new SendMessage(chatId.toString(), "Chào mừng " + name + ". \nĐể biết thêm các tính năng. \nVui lòng nhấn /help"));
    }

    public void help(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        String start = Constant.HELP;
        silentSender.execute(new SendMessage(chatId.toString(), start));
        log.info("{}" , chatStates.get(chatId.toString()));
        chatStates.put(chatId.toString(), UserState.HELP);
    }

    public void search(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        String start = Constant.SEARCH;
        silentSender.execute(new SendMessage(chatId.toString(), start));
        log.info("{}" , chatStates.get(chatId.toString()));
        chatStates.put(chatId.toString(), UserState.SEARCH);
    }

    public void replyTablePerson(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        SendMessage sendMessage = new SendMessage(chatId.toString(), BuildTableUtils.buildTable(StaticObjects.people, Person.class));
        sendMessage.enableHtml(true);
        silentSender.execute(sendMessage);
    }

    public void replyTableOrder(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        SendMessage sendMessage = new SendMessage(chatId.toString(), BuildTableUtils.buildTable(StaticObjects.orders, Order.class));
        sendMessage.enableHtml(true);
        silentSender.execute(sendMessage);
    }

}
