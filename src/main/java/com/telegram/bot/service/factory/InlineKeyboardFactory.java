package com.telegram.bot.service.factory;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.Arrays;
import java.util.List;

public class InlineKeyboardFactory {
    public static InlineKeyboardMarkup keyboardMarkup () {
        InlineKeyboardButton pizza = new InlineKeyboardButton("Pizza");
        pizza.setCallbackData("pizza");
        InlineKeyboardButton drink = new InlineKeyboardButton("Drink");
        pizza.setCallbackData("drink");
        List<InlineKeyboardButton> inlineKeyboardButtons = Arrays.asList(pizza, drink);
        return new InlineKeyboardMarkup(Arrays.asList(inlineKeyboardButtons));
    }
}
