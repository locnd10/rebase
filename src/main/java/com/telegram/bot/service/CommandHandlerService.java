package com.telegram.bot.service;

import com.telegram.bot.service.factory.InlineKeyboardFactory;
import com.telegram.bot.utils.Constant;
import com.telegram.bot.utils.UserState;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.telegram.abilitybots.api.sender.MessageSender;
import org.telegram.abilitybots.api.sender.SilentSender;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;

import java.util.Map;

@Slf4j
@Component
public class CommandHandlerService {

    public String commandHandler(SilentSender silentSender, MessageSender messageSender, Map<String, Object> chatStates, String command, Long chatId) {
        switch (command) {
            case "/start":
                start(silentSender, chatStates, chatId);
                break;
            case "/help":
                help(silentSender, chatStates, chatId);
                break;
            case "/search":
               search(silentSender, chatStates, chatId);
               break;
            case "/stop":
                stop(silentSender, chatStates, chatId);
                break;
            case "/order":
                order(silentSender, chatStates, chatId);
                break;

        }

        // If the command is not recognized, return a default response
        return "Command not recognized. Use /help to see available commands.";
    }

    public void start(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        String start = Constant.START_TEXT;
        silentSender.execute(new SendMessage(chatId.toString(), start));
        log.info("{}" , chatStates.get(chatId.toString()));
        chatStates.put(chatId.toString(), UserState.START);
    }

    public void help(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        String start = Constant.HELP;
        silentSender.execute(new SendMessage(chatId.toString(), start));
        log.info("{}" , chatStates.get(chatId.toString()));
        chatStates.put(chatId.toString(), UserState.HELP);
    }

    public void search(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        String search = Constant.SEARCH;
        silentSender.execute(new SendMessage(chatId.toString(), search));
        log.info("{}" , chatStates.get(chatId.toString()));
        chatStates.put(chatId.toString(), UserState.SEARCH);
    }

    public void stop(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        String stop = Constant.STOP;
        silentSender.execute(new SendMessage(chatId.toString(), stop));
        log.info("{}" , chatStates.get(chatId.toString()));
        chatStates.put(chatId.toString(), UserState.STOP);
    }
    public void order(SilentSender silentSender, Map<String, Object> chatStates, Long chatId) {
        String order = Constant.ORDER;
        SendMessage sendMessage = new SendMessage();
        sendMessage.setText(order);
        sendMessage.setChatId(chatId.toString());
        sendMessage.setReplyMarkup(InlineKeyboardFactory.keyboardMarkup());
        try {
            silentSender.execute(sendMessage);
            log.info("{}", chatStates.get(chatId.toString()));
            chatStates.put(chatId.toString(), UserState.ORDER);
        } catch (Exception e) {
            log.error("Exception: ", e);
        }
    }


}
